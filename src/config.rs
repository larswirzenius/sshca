//! SSH CA tool configuration handling.

use crate::error::CAError;
use crate::util::read;

use directories_next::ProjectDirs;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

const APPLICATION: &str = "sshca";
const DEFAULT_PASS_KEY: &str = "sshca.store";

#[derive(Debug, Default, Deserialize)]
struct TentativeConfig {
    pub store: Option<PathBuf>,
    pub how: Option<String>,
    pub pass: Option<String>,
}

/// Representation of SSH CA tool configuration.
#[derive(Debug, Serialize)]
pub struct Config {
    /// Path to file in which keys and other things are store.
    pub store: PathBuf,

    /// How is the store stored persistently?
    pub how: StoreHow,

    /// Name (key) for the pass command for persisting the key store.
    pub pass_key: String,
}

/// Supported ways how to persist the store.
#[derive(Debug, Serialize)]
pub enum StoreHow {
    /// Store in a cleartext file.
    #[serde(rename = "file")]
    File,

    /// Store using the [`pass`](https://www.passwordstore.org/) command.
    #[serde(rename = "pass")]
    Pass,
}

/// Build a [`Config`].
pub struct ConfigBuilder {
    dirs: ProjectDirs,
    tentative: TentativeConfig,
}

impl ConfigBuilder {
    /// Create a new configuration builder.
    pub fn new(filename: &Option<PathBuf>) -> Result<Self, CAError> {
        let dirs = dirs();

        let filename = if let Some(filename) = filename {
            filename.clone()
        } else {
            dirs.config_dir().join("config.yaml")
        };

        let tentative = if filename.exists() {
            let yaml = read(filename)?;
            serde_yaml::from_slice(&yaml).map_err(CAError::YamlError)?
        } else {
            TentativeConfig::default()
        };

        Ok(Self { dirs, tentative })
    }

    /// Create a new [`Config`].
    pub fn build(&self) -> Result<Config, CAError> {
        Ok(Config {
            store: if let Some(store) = &self.tentative.store {
                store.to_path_buf()
            } else {
                self.dirs.data_dir().join("store.yaml")
            },
            how: if let Some(how) = &self.tentative.how {
                match how.as_ref() {
                    "file" => StoreHow::File,
                    "pass" => StoreHow::Pass,
                    _ => return Err(CAError::UnknownHow(how.into())),
                }
            } else {
                StoreHow::File
            },
            pass_key: self
                .tentative
                .pass
                .as_deref()
                .unwrap_or(DEFAULT_PASS_KEY)
                .into(),
        })
    }

    /// Set store file.
    pub fn store(&mut self, store: &Option<PathBuf>) -> &Self {
        if store.is_some() {
            self.tentative.store.clone_from(store);
        }
        self
    }
}

fn dirs() -> ProjectDirs {
    if let Some(dirs) = ProjectDirs::from("", "", APPLICATION) {
        dirs
    } else {
        panic!("can't figure out the configuration directory");
    }
}
