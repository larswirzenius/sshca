//! Display SSH CA tool configuration.

use crate::cmd::Runnable;
use crate::config;
use crate::error::CAError;
use crate::store::KeyStore;
use clap::Parser;

/// Display configuration.
#[derive(Debug, Parser)]
pub struct Config {}

impl Runnable for Config {
    fn run(&mut self, config: &config::Config, _store: &mut KeyStore) -> Result<(), CAError> {
        serde_yaml::to_writer(&std::io::stdout(), config).map_err(CAError::YamlError)?;
        Ok(())
    }
}
