//! Implementations of commands for SSH CA tool.

use crate::config::Config;
use crate::error::CAError;
use crate::store::KeyStore;

/// A thing that can be run as a command.
pub trait Runnable {
    /// Run the command.
    fn run(&mut self, config: &Config, store: &mut KeyStore) -> Result<(), CAError>;
}

pub mod ca;
pub mod config;
pub mod host;
pub mod user;
