# sshca---SSH certificate management

The `sshca` tool helps manage an SSH Certificate Authority ([SSH CA][]) and
create host and user certificates. Such certificates make using and
administering SSH less tedious and more secure.

An SSH CA is an SSH key dedicated to signing, or certifying, other SSH
keys. Such a signed key is called a certificate and is used together
with the private part of the certified key. The certificate is used
instead of the public key.

SSH clients and servers can be configured to trust certificates made
by one or more CA keys. This makes it possible for a client to trust a
server without asking the user to accept the host key for each new
server. A server can trust a client without having the client's public
key configured for that user in the `authorized_key` file. This
simplifies overall key management significantly, but requires creating
and managing CA keys and certificates.

[SSH CA]: https://liw.fi/sshca

See <https://sshca.liw.fi/> for more information about the tool.

## Dependencies

You need the following to run `./check` and build and run the
software:

* the Rust toolchain: `cargo` and `rustc` and anything they need
* Python version 3
* [Subplot](https://subplot.tech/)
* [Pandoc](https://pandoc.org/)
* pdflatex, such as from [TeXlive](https://tug.org/texlive/)
