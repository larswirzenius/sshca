// Implementations of Subplot scenario steps for sshca.md.

use subplotlib::steplibrary::runcmd::Runcmd;

use std::path::Path;

#[derive(Debug, Default)]
struct SubplotContext {}

impl ContextElement for SubplotContext {}

#[step]
#[context(SubplotContext)]
#[context(Runcmd)]
fn install_sshca(context: &ScenarioContext) {
    let target_exe = env!("CARGO_BIN_EXE_sshca");
    let target_path = Path::new(target_exe);
    let target_path = target_path.parent().ok_or("No parent?")?;
    context.with_mut(
        |context: &mut Runcmd| {
            context.prepend_to_path(target_path);
            Ok(())
        },
        false,
    )?;
}
